<?php

function home_videos_post_type() {

	$labels = array(
		'name'                => _x( 'Video Boxes', 'Post Type General Name', 'babcockpartners' ),
		'singular_name'       => _x( 'Video Box', 'Post Type Singular Name', 'babcockpartners' ),
		'menu_name'           => __( 'Video Boxes', 'babcockpartners' ),
		'name_admin_bar'      => __( 'Video Boxes for the homepage', 'babcockpartners' ),
		'parent_item_colon'   => __( 'Parent Video:', 'babcockpartners' ),
		'all_items'           => __( 'All Videos', 'babcockpartners' ),
		'add_new_item'        => __( 'Add New Video', 'babcockpartners' ),
		'add_new'             => __( 'Add New', 'babcockpartners' ),
		'new_item'            => __( 'New Video', 'babcockpartners' ),
		'edit_item'           => __( 'Edit Video', 'babcockpartners' ),
		'update_item'         => __( 'Update Video', 'babcockpartners' ),
		'view_item'           => __( 'View Video', 'babcockpartners' ),
		'search_items'        => __( 'Search Video', 'babcockpartners' ),
		'not_found'           => __( 'Not found', 'babcockpartners' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'babcockpartners' ),
	);
	$args = array(
		'label'               => __( 'Video Box', 'babcockpartners' ),
		'description'         => __( 'Homepage Video Boxes', 'babcockpartners' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-video-alt',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,		
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'video_box', $args );

}
add_action( 'init', 'home_videos_post_type', 0 );

// Reviews Custom Post Type
function reviews_post_type() {

	$labels = array(
		'name'                => _x( 'Reviews', 'Post Type General Name', 'babcock' ),
		'singular_name'       => _x( 'Review', 'Post Type Singular Name', 'babcock' ),
		'menu_name'           => __( 'Reviews', 'babcock' ),
		'name_admin_bar'      => __( 'Reviews', 'babcock' ),
		'parent_item_colon'   => __( 'Parent Review:', 'babcock' ),
		'all_items'           => __( 'All Reviews', 'babcock' ),
		'add_new_item'        => __( 'Add New Review', 'babcock' ),
		'add_new'             => __( 'Add New', 'babcock' ),
		'new_item'            => __( 'New Review', 'babcock' ),
		'edit_item'           => __( 'Edit Review', 'babcock' ),
		'update_item'         => __( 'Update Review', 'babcock' ),
		'view_item'           => __( 'View Review', 'babcock' ),
		'search_items'        => __( 'Search Reviews', 'babcock' ),
		'not_found'           => __( 'Not found', 'babcock' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'babcock' ),
	);
	$args = array(
		'label'               => __( 'Review', 'babcock' ),
		'description'         => __( 'Reviews from the web', 'babcock' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'custom-fields', ),
		'taxonomies'          => array( 'category' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-star-filled',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,		
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'reviews', $args );

}
add_action( 'init', 'reviews_post_type', 0 );