<?php get_header(); ?>
<section id="breadcrumbs" class="hidden-xs">
	<div class="container">
		<div class="row">
			<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
				yoast_breadcrumb();
				} ?>
		</div>
	</div>
</section>
<section id="main-content">
	<div class="container">
		<div class="row">
			<div class="col-sm-8">
				<h1><?php the_title(); ?></h1>
				<div class="row-fluid"><?php get_template_part('partials/page-images'); ?></div>
				<?php if(have_posts()):while(have_posts()):the_post(); ?>
				<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
			<div class="col-sm-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
