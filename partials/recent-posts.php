<h3>BABCOCK PARTNERS NEWS</h3>
<ul>
	<?php

	$args = array( 'numberposts' => '3' );
	$recent_posts = wp_get_recent_posts( $args );
	foreach( $recent_posts as $recent) {
		echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li><hr /> ';
	} ?>
</ul>