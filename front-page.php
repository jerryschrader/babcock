<?php get_header(); ?>
<section id="splash" class="hidden-xs">
	<div class="container">
		<div class="row">
			<div class="col-sm-5">
			<h3 class="animate-in">LOUISIANA <br /><span> SOUTHPAW</span></h3>
				<img src="<?php echo get_template_directory_uri(); ?>/images/splash-left.jpg" alt="" class="splash-left">
			</div><!--/col-sm-4 -->
				<div class="col-sm-4 stephen pull-right">
					<img src="<?php echo get_template_directory_uri(); ?>/images/steve-clear.png" alt="Babcock Partners" class="stephen-image" />
				</div>
		</div>
	</div>
</section>
<section id="main-content">
	<div class="container">
		<div class="row">
			<div class="col-sm-8">

			<!-- begin homepage videos -->
			<div class="row-fluid">
				<?php get_template_part('partials/homepage-videos'); ?>
			</div>
			<!--/homepage videos-->

				<?php if(have_posts()):while(have_posts()):the_post(); ?>

				<?php the_content(); ?>

				<?php endwhile; endif; ?>
			</div>
			<div class="col-sm-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>
<section id="other-areas">
	<?php get_template_part('partials/other-areas'); ?>
</section>
<?php get_footer(); ?>
