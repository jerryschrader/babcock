<?php

	function build_widgets() {

		register_sidebar( array(
				'name' => __('Main Sidebar', 'babcock'),
				'description' => __('This is the main sidebar','babcock'),
				'id' => 'main-sidebar',
				'before_title' => '<h3>',
				'after_title' => '</h3>',
				'before_widget' => '',
				'after_widget' => '',
		));
		register_sidebar( array(
				'name' => __('Upper Footer One', 'babcock'),
				'description' => __('This is the left upper footer widget','babcock'),
				'id' => 'upper-footer-one',
				'before_title' => '<h3>',
				'after_title' => '</h3>',
				'before_widget' => '',
				'after_widget' => '',
		));
		register_sidebar( array(
				'name' => __('Upper Footer Two', 'babcock'),
				'description' => __('This is the 2nd upper footer widget','babcock'),
				'id' => 'upper-footer-two',
				'before_title' => '<h3>',
				'after_title' => '</h3>',
				'before_widget' => '',
				'after_widget' => '',
		));
		register_sidebar( array(
				'name' => __('Upper Footer Three', 'babcock'),
				'description' => __('This is the 3rd upper footer widget','babcock'),
				'id' => 'upper-footer-three',
				'before_title' => '<h3>',
				'after_title' => '</h3>',
				'before_widget' => '',
				'after_widget' => '',
		));
		register_sidebar( array(
				'name' => __('Header Phone Number', 'babcock'),
				'description' => __('This is the header phone number widget','babcock'),
				'id' => 'header-phone',
				'before_title' => '',
				'after_title' => '',
				'before_widget' => '',
				'after_widget' => '',
		));
		

	}
	add_action('widgets_init', 'build_widgets');