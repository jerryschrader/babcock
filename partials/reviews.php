<h3 class="text-center">Check Out Our Reviews!</h3>
<div class="col-lg-12">
<div class="carousel slide" id="myCarousel">
  <div class="carousel-inner">
  

<?php
$args = array('post_type' => 'reviews');
$query = new WP_Query($args);
$activeClass = 'active';

if($query->have_posts()):while($query->have_posts()):$query->the_post(); ?>

    <div class="item <?php echo $activeClass; $activeClass =' '; ?>">
      <div class="col-lg-3">
        <h3><?php the_title(); ?></h3>
        <p><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></p>
        <p><?php echo '<p>' . wp_trim_words( get_the_content(), 25 ) . '</p>'; ?></p>
        <p><small><?php get_post_meta($post->ID, 'Date', true); ?></small></p>
      </div>
    </div>
<?php endwhile; wp_reset_postdata(); endif; ?>


  </div>
</div>
</div>