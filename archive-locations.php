<?php get_header(); ?>
<div class="container">
	<div class="row">
		<div class="col-sm-8">
			<div class="row-fluid">

				<?php if(have_posts()):while(have_posts()):the_post(); ?>

				<?php the_excerpt(); ?>

				<?php endwhile; endif; ?>

			</div>
		</div>
		<div class="col-sm-4">
			<?php the_sidebar(); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>