<style>
/* Hover Transition */
 
#other-areas .wrapper {
  overflow: hidden;
  display: inline-block;
  position: relative;
  height: 200px;
}
  #other-areas .wrapper:hover .overlay {
    right: 200px;
  }
  #other-areas .wrapper:hover .description {
    /* IE 8 */
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    /* IE 5.5-7 */
    filter: alpha(opacity=100);
    /* Netscape */
    -moz-opacity: 1;
    /* Safari 1.x */
    -khtml-opacity: 1;
    /* Modern browsers */
    opacity: 1;
 
    transition: 1s ease-in;
    -webkit-transition: 1s ease-in;
  }
 
  #other-areas .wrapper .overlay {
    display: block;
    position: absolute;
    height: 200px;
    width: 100%;
    top: 0;
    right: -100%;
 
    transition: .4s ease-out;
    -webkit-transition: .4s ease-out;
  }
    #other-areas .wrapper .overlay .rectangle,
    #other-areas .wrapper .overlay .arrow-left {
      position: absolute;
      top: 0;
    }
    #other-areas .wrapper .overlay .rectangle {
      display: block;
      margin-left: 200px;
      width: 100%;
      height: 100%;
      background-color: rgba(240, 144, 51,.8);
    }
    #other-areas .wrapper .overlay .arrow-left {
      position: relative;
      left: 0;
      width: 0; 
      height: 0; 
      top: 0;
      bottom: 0;
 
      border-bottom: 200px solid transparent; 
      border-right: 200px solid rgba(240, 144, 51,.8); 
    }
 
  #other-areas .wrapper .description {
    text-align: left;
    position: absolute;
    padding: 30px 50px;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    color: #fff;
 
    /* IE 8 */
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
    /* IE 5.5-7 */
    filter: alpha(opacity=0);
    /* Netscape */
    -moz-opacity: 0;
    /* Safari 1.x */
    -khtml-opacity: 0;
    /* Modern browsers */
    opacity: 0;
 
    transition: .2s ease-out;
    -webkit-transition: .2s ease-out;
  }
    #other-areas .wrapper .description h3 {
      text-align: center;
      font-size: 20px;
      margin-bottom: 25px;
      font-weight: bold;
    }
 
  #other-areas .wrapper .link {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 10;
    margin: 0;
  }
 
  #other-areas .wrapper img {
    vertical-align: bottom;
  }
</style>
<div class="container">
	<div class="row">
		<div class="col-sm-3">
			<div class="wrapper">
			  <a class="link" href="#"></a>
			  <div class="overlay">
			    <div class="arrow-left"></div>
			    <div class="rectangle"></div>
			  </div>
			 
			  <div class="description">
			    <h3>WORKERS <br />COMPENSATION</h3>
			  </div>
			  <img src="<?php echo get_template_directory_uri(); ?>/images/other-areas/workers-comp.jpg">
			</div><!--/wrapper -->
		</div><!--/col-sm-3-->
		<div class="col-sm-3">
			<div class="wrapper">
			  <a class="link" href="#"></a>
			  <div class="overlay">
			    <div class="arrow-left"></div>
			    <div class="rectangle"></div>
			  </div>
			 
			  <div class="description">
			    <h3>WRONGFUL <br />DEATH</h3>
			  </div>
			  <img src="<?php echo get_template_directory_uri(); ?>/images/other-areas/wrongful-death.jpg">
			</div><!--/wrapper -->
		</div><!--/col-sm-3-->
		<div class="col-sm-3">
			<div class="wrapper">
			  <a class="link" href="#"></a>
			  <div class="overlay">
			    <div class="arrow-left"></div>
			    <div class="rectangle"></div>
			  </div>
			 
			  <div class="description">
			    <h3>DWI / DUI</h3>
			  </div>
			  <img src="<?php echo get_template_directory_uri(); ?>/images/other-areas/dwi.jpg">
			</div><!--/wrapper -->
		</div><!--/col-sm-3-->
		<div class="col-sm-3">
			<div class="wrapper">
			  <a class="link" href="#"></a>
			  <div class="overlay">
			    <div class="arrow-left"></div>
			    <div class="rectangle"></div>
			  </div>
			 
			  <div class="description">
			    <h3>BRAIN <br /> INJURY</h3>
			  </div>
			  <img src="<?php echo get_template_directory_uri(); ?>/images/other-areas/brain-injury.jpg">
			</div><!--/wrapper -->
		</div><!--/col-sm-3-->
	</div><!--/row -->

	<div class="row">
		<div class="col-sm-3">
			<div class="wrapper">
			  <a class="link" href="#"></a>
			  <div class="overlay">
			    <div class="arrow-left"></div>
			    <div class="rectangle"></div>
			  </div>
			 
			  <div class="description">
			    <h3>FLOOD <br /> INSURANCE</h3>
			  </div>
			  <img src="<?php echo get_template_directory_uri(); ?>/images/other-areas/flood-insurance.jpg">
			</div><!--/wrapper -->
		</div><!--/col-sm-3-->
		<div class="col-sm-3">
			<div class="wrapper">
			  <a class="link" href="#"></a>
			  <div class="overlay">
			    <div class="arrow-left"></div>
			    <div class="rectangle"></div>
			  </div>
			 
			  <div class="description">
			    <h3>OIL FIELD <br /> INJURIES</h3>
			  </div>
			  <img src="<?php echo get_template_directory_uri(); ?>/images/other-areas/oil-field-injury.jpg">
			</div><!--/wrapper -->
		</div><!--/col-sm-3-->
		<div class="col-sm-3">
			<div class="wrapper">
			  <a class="link" href="#"></a>
			  <div class="overlay">
			    <div class="arrow-left"></div>
			    <div class="rectangle"></div>
			  </div>
			 
			  <div class="description">
			    <h3>CRIMINAL <br />DEFENSE</h3>
			  </div>
			  <img src="<?php echo get_template_directory_uri(); ?>/images/other-areas/criminal-defense.jpg">
			</div><!--/wrapper -->
		</div><!--/col-sm-3-->
		<div class="col-sm-3">
			<div class="wrapper">
			  <a class="link" href="#"></a>
			  <div class="overlay">
			    <div class="arrow-left"></div>
			    <div class="rectangle"></div>
			  </div>
			 
			  <div class="description">
			    <h3>BURN <br />VICTIMS</h3>
			  </div>
			  <img src="<?php echo get_template_directory_uri(); ?>/images/other-areas/burn-victim.jpg">
			</div><!--/wrapper -->
		</div><!--/col-sm-3-->
	</div><!--/row -->
</div><!--/container-->