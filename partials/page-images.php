<style>
	*{
  margin: 0;
  padding: 0;
	outline: none;
	border: none;
	box-sizing: border-box;
}

*:before,
*:after{-webkit-box-sizing: border-box;}

ul{list-style: none; margin: 0 auto; display: table;}
li{display: inline-block; margin: 20px 20px;}

img{
	border: solid 10px black;
	transition: all .3s ease;
}
.overlay li{position: relative;}

.overlay-transition{
	width: 100%;
	height:100%;
	position: absolute;
	top: 0;
	cursor: pointer;
}

.overlay-transition ul{position: relative;}

.overlay-transition ul li{
	display: list-item;
	float: left;
	margin:0;
}

.overlay-item{
	width: 32px;
	height: 23px;
	background: rgba(0,0,0,.5);
	transition: all .4s ease-in-out;
}

.overlay-transition:hover ul li div{background: transparent;}
.delay-1{-webkit-transition-delay: .04s;}
.delay-2{-webkit-transition-delay: .08s;}
.delay-3{-webkit-transition-delay: .12s;}
.delay-4{-webkit-transition-delay: .16s;}
.delay-5{-webkit-transition-delay: .20s;}
.delay-6{-webkit-transition-delay: .24s;}
.delay-7{-webkit-transition-delay: .28s;}
.delay-8{-webkit-transition-delay: .32s;}
.delay-9{-webkit-transition-delay: .36s;}
.delay-10{-webkit-transition-delay: .40s;}
</style>

<div class="wrap">
  	<ul class="overlay">
			<li>
				<img src="<?php the_field('page_image'); ?>" class="transition" />
				<div class="overlay-transition">
					<ul>
						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>

						<li><div class="overlay-item delay-1"></div></li>
						<li><div class="overlay-item delay-2"></div></li>
						<li><div class="overlay-item delay-3"></div></li>
						<li><div class="overlay-item delay-4"></div></li>
						<li><div class="overlay-item delay-5"></div></li>
						<li><div class="overlay-item delay-6"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-9"></div></li>
						<li><div class="overlay-item delay-10"></div></li>
						<li><div class="overlay-item delay-7"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						<li><div class="overlay-item delay-8"></div></li>
						


					</ul>
				</div>
			</li>
		</ul>
	</div>