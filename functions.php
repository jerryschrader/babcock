<?php

function load_styles() {
	wp_enqueue_style('bootstrap_styles', get_template_directory_uri() . '/css/bootstrap.css');
	wp_enqueue_style('wordpress_styles', get_template_directory_uri() . '/css/wordpress.css');
	wp_enqueue_style('animate_styles', get_template_directory_uri() . '/css/animate.css');
	wp_enqueue_style('fontawesome_styles', get_template_directory_uri() . '/fonts/font-awesome/css/font-awesome.min.css');
	wp_enqueue_style('google-fonts', 'http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic');
	wp_enqueue_style( 'custom-styles', get_template_directory_uri() . '/styles.css');
}
add_action('wp_enqueue_scripts', 'load_styles');


function load_scripts() {
	wp_enqueue_script('bootstrap_scripts', get_template_directory_uri() . '/js/bootstrap.js', array('jquery'), '1.0', true);
	wp_enqueue_script('custom_scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0', true);
}
add_action('wp_enqueue_scripts', 'load_scripts');

// navigation
require_once('wp_bootstrap_navwalker.php');

register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'lavane' ),
) );

add_theme_support( 'post-thumbnails' );
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

@include('partials/custom-post-types.php');
@include('partials/widgets.php');