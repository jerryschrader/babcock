</div><!--/sticky footer, begins in header.php -->

<section id="reviews">
	<div class="container">
		<?php get_template_part('partials/reviews'); ?>
	</div>
</section><!--/reviews-->

<section id="trust">
	<?php get_template_part('partials/trust'); ?>
</section><!--/trust-->

<section id="footer-upper">
	<div class="container">
		<div class="row">
			<div class="col-sm-4 recent-posts-widget">
				<?php get_template_part('partials/recent-posts'); ?>
			</div>
			<div class="col-sm-4">
				<?php dynamic_sidebar('upper-footer-two'); ?>
			</div>
			<div class="col-sm-4">
				<?php dynamic_sidebar('upper-footer-three'); ?>
			</div>
		</div>
		<div class="shadow-line"></div>
	</div>
</section>
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-8">
				<span>&copy; <?php echo date('Y') ?> Babcock Partners, LLC - All of Babcock Partners, LLC's personal injury lawyers are licensed in Louisiana. One or more injury lawyers are licensed in all federal district courts in Louisiana, the United States Court of Appeals for the Fifth Circuit (which covers the state of Louisiana), and the Supreme Court of the United States. Babcock Partners, LLC's office is in Baton Rouge, Louisiana. The accident attorney responsible for this website is Stephen Babcock.</span>
			</div>
			<div class="col-sm-4">
				<a href="http://www.facebook.com/babcockpartners"><img src="<?php echo get_template_directory_uri(); ?>/images/social/facebook.png" alt="Babcock Partners Facebook"></a>
				<a href="http://www.twitter.com/babcockpartners"><img src="<?php echo get_template_directory_uri(); ?>/images/social/twitter.png" alt="Babcock Partners Twitter"></a>
				<a href="https://plus.google.com/110476072248129782161/"><img src="<?php echo get_template_directory_uri(); ?>/images/social/googleplus.png" alt="Babcock Partners Google Plus"></a>
				<a href="http://www.linkedin.com/companies/babcock-partners-llc"><img src="<?php echo get_template_directory_uri(); ?>/images/social/linkedin.png" alt="Babcock Partners LinkedIn"></a>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>