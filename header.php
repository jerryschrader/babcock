<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php wp_title(); ?></title>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
      <!-- Begin sticky footer, ends in footer.php -->
  <div id="wrap">
  <section id="top">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-lg-4">
          <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Babcock Partners LLC" class="logo-top" />
        </div>
        <div class="col-sm-6 col-lg-4 hidden-xs hidden-sm hidden-md text-center header-middle">FREE CONSULTATION <br /> <span>PAY NOTHING UNLESS YOU WIN</span></div>
        <div class="col-lg-3 hidden-xs pull-right"><h3 class="top-phone-desktop">(225) 819-3737</h3></div>
      </div>
    </div>
  </section><!--/top -->
  <section id="navigation">
    <div class="container">
      <div class="row clearfix">
        <div class="col-xs-6">
          <?php get_template_part('partials/menu'); ?>
        </div>
        <div class="col-xs-6 visible-xs">
          <span class="top-phone">(225) 819-3737</span>
        </div>
      </div>
    </div>
  </section>